// reed-solomone.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <map>
#include <vector>
#include <iostream>
#include <utility>
#include <map>
namespace Mod
{
	inline long mod(long a, long b)
	{
		return (a % b + b) % b;
	}
	inline long inverse(long a, long b, long mod)
	{
		for (int i = 1; i < mod; i++)
		{
			if (Mod::mod(b * i, mod) == a)
				return i;
		}
	}
}

class element
{
public:
	element() : x_num(0), a_num(0) {}
	element(int x, int a) : x_num(x), a_num(a) {}
	int x_num;
	int a_num;
};

int main()
{
	std::map<int, std::vector<int>> GF = {};
	GF.emplace(0,	std::vector<int>({ 0, 0, 0, 1 }));
	GF.emplace(1,	std::vector<int>({ 0, 0, 1, 0 }));
	GF.emplace(2,	std::vector<int>({ 0, 1, 0, 0 }));
	GF.emplace(3,	std::vector<int>({ 1, 0, 0, 0 }));
	GF.emplace(4,	std::vector<int>({ 0, 0, 1, 1 }));
	GF.emplace(5,	std::vector<int>({ 0, 1, 1, 0 }));
	GF.emplace(6,	std::vector<int>({ 1, 1, 0, 0 }));
	GF.emplace(7,	std::vector<int>({ 1, 0, 1, 1 }));
	GF.emplace(8,	std::vector<int>({ 0, 1, 0, 1 }));
	GF.emplace(9,	std::vector<int>({ 1, 0, 1, 0 }));
	GF.emplace(10,	std::vector<int>({ 0, 1, 1, 1 }));
	GF.emplace(11,	std::vector<int>({ 1, 1, 1, 0 }));
	GF.emplace(12,	std::vector<int>({ 1, 1, 1, 1 }));
	GF.emplace(13,	std::vector<int>({ 1, 1, 0, 1 }));
	GF.emplace(14,	std::vector<int>({ 1, 0, 0, 1 }));

	std::vector<element> vec1 = { {1, 0}, {0, 1} };
	std::vector<element> vec2 = { {1, 0}, {0, 2} };

	for (int sigma = 3; sigma <= 15; sigma++)
	{
		std::vector<element> vec_res;
		std::vector<element> total_res;
		for (int i = 0; i < vec1.size(); i++)
		{
			for (int j = 0; j < vec2.size(); j++)
			{
				element res_element = { vec1[i].x_num + vec2[j].x_num , Mod::mod(vec1[i].a_num + vec2[j].a_num, 15)};
				vec_res.emplace_back(res_element);
			}
		}

		total_res.emplace_back(vec_res[0]);

		for (int i = 1; i < vec_res.size() - 1; i += 2)
		{
			element elem1 = vec_res[i];
			element elem2 = vec_res[i + 1];
			element res_elem = {elem1.x_num, 0};

			auto gf_vec1 = GF[elem1.a_num];
			auto gf_vec2 = GF[elem2.a_num];
			std::vector<int> res;

			for (int k = 0; k < gf_vec2.size(); k++)
			{
				int sum = gf_vec2[k] + gf_vec1[k];

				if (sum == 2)
					sum = 0;

				res.emplace_back(sum);
			}

			int res_alpha = 0;
			// Parse GF once again to find alpha
			for (auto& element : GF)
			{
				int number_of_correct = 0;
				for (int k = 0; k < res.size(); k++)
				{
					if (res[k] == element.second[k])
					{
						number_of_correct++;
						if (number_of_correct == res.size())
						{
							res_alpha = element.first;
						}
					}
					else
					{
						break;
					}
				}
			}

			res_elem.a_num = res_alpha;
			total_res.emplace_back(res_elem);
		}
		total_res.emplace_back(vec_res[vec_res.size() - 1]);

		for (auto& elem : total_res)
		{
			std::cout << "a^" << elem.a_num << "x^" << elem.x_num << "+";
		}
		std::cout << std::endl;

		vec1.resize(total_res.size());
		for (int i = 0; i < total_res.size(); i++)
		{
			vec1[i].x_num = total_res[i].x_num;
			vec1[i].a_num = total_res[i].a_num;
		}
		vec2[vec2.size() - 1].a_num += 1;
	}

	/*int current_x_number = vec_res[0].x_num;
	while (current_x_number >= 0)
	{
		element res_elem = { current_x_number, 0 };
		for (int i = 0; i < vec_res.size(); i++)
		{
			element elem = vec_res[i];
			if (elem.x_num == current_x_number)
			{
				if (res_elem.a_num == 0)
				{
					if (elem.x_num == 2)
					{
						std::cout << std::endl;
					}
					// If first -- add as normal
					res_elem.a_num += elem.a_num;
				}
				else
				{
					if (elem.x_num == 2)
					{
						std::cout << std::endl;
					}
					//Do proper GF addition
					auto gf_vec1 = GF[elem.a_num];
					auto gf_vec2 = GF[res_elem.a_num];
					std::vector<int> res;

					for (int k = 0; k < gf_vec2.size(); k++)
					{
						int sum = gf_vec2[k] + gf_vec1[k];

						if (sum == 2)
							sum = 0;

						res.emplace_back(sum);
					}

					int res_alpha = 0;
					// Parse GF once again to find alpha
					for (auto& element : GF)
					{
						int number_of_correct = 0;
						for (int k = 0; k < res.size(); k++)
						{
							if (res[k] == element.second[k])
							{
								number_of_correct++;
								if (number_of_correct == res.size())
								{
									res_alpha = element.first;
								}
							}
							else
							{
								break;
							}
						}
					}

					res_elem.a_num = res_alpha;
			
				}
			}
		}
		current_x_number--;
		total_res.emplace_back(res_elem);
	}*/

	/* for (int i = 0; i < vec1.size(); i++)
	{
		auto res1 = GF[vec1[i]];
		for (int j = 0; j < vec2.size(); j++)
		{
			auto res2 = GF[vec2[j]];
			std::vector<int> res;
			if ( vec1[i] == 0 )
			{
				if (vec2[j] == 0)
				{
					vec_res.emplace_back(0);
				}
				else
				{
					vec_res.emplace_back(vec2[j]);
				}
				continue;
			}
			else if (vec2[j] == 0)
			{
				vec_res.emplace_back(vec1[i]);
				continue;
			}

			for (int k = 0; k < res2.size(); k++)
			{
				int sum = res2[k] + res1[k];

				if (sum == 2)
					sum = 0;

				res.emplace_back(sum);
			}

			int res_alpha = 0;
			// Parse GF once again to find alpha
			for (auto& elem : GF)
			{
				int number_of_correct = 0;
				for (int k = 0; k < res.size(); k++)
				{
					if (res[k] == elem.second[k])
					{
						number_of_correct++;
						if (number_of_correct == res.size())
						{
							res_alpha = elem.first;
						}
					}
					else
					{
						break;
					}
				}
			}

			vec_res.emplace_back(res_alpha);
		}
	} */

}

